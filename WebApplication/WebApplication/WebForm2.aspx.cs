﻿
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImageProcessor.ConvertGrayToWhite("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg", "E:\\图片\\WebApplication\\WebApplication\\images\\opencv.jpg");
        }

        public class ImageProcessor
        {
            public static void ConvertGrayToWhite(string inputImagePath, string outputImagePath, double threshold = 50)
            {
                Mat src = new Mat(inputImagePath, ImreadModes.Grayscale);
                Mat dst = new Mat();

                // 阈值化处理，将灰色部分变成白色
                Cv2.Threshold(src, dst, 0, 255, ThresholdTypes.BinaryInv | ThresholdTypes.Otsu);

                // 保存处理后的图片
                dst.SaveImage(outputImagePath);

                // 释放资源
                src.Dispose();
                dst.Dispose();
            }
        }
    }
}