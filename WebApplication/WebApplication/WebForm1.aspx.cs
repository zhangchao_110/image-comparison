﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using static System.Net.Mime.MediaTypeNames;
using System.Drawing.Imaging;

namespace WebApplication
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            #region 检查每个像素的颜色，如果它的灰度值在某个阈值范围内，则将其替换为白色
            Bitmap originalImage5 = (Bitmap)System.Drawing.Image.FromFile("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg");
            Bitmap imageWithWhiteBackground = ChangeGrayBackgroundToWhite(originalImage5, 10);
            imageWithWhiteBackground.Save("E:\\图片\\WebApplication\\WebApplication\\images\\灰色判断.jpg");
            #endregion


            #region 改变对比度
            System.Drawing.Image originalImage4 = System.Drawing.Image.FromFile("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg");
            Bitmap adjustedImage = AdjustContrast(originalImage4, 1.3f); // 增加对比度
            adjustedImage.Save("E:\\图片\\WebApplication\\WebApplication\\images\\对比1.3.jpg");
            #endregion


            //背景改为白色
            ChangeBackgroundToWhite("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg", "E:\\图片\\WebApplication\\WebApplication\\images\\白1.jpg");

            #region 彩色图片灰度
            Bitmap originalImage3 = (Bitmap)System.Drawing.Image.FromFile("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg");
            Bitmap newImage3 = MakeGrayscale(originalImage3);
            newImage3.Save("E:\\图片\\WebApplication\\WebApplication\\images\\灰色.png", System.Drawing.Imaging.ImageFormat.Png);
            #endregion

            #region 图片透明
            System.Drawing.Image originalImage2 = System.Drawing.Image.FromFile("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg");
            Color transparentColor = Color.White; // 假设你想要的透明颜色是白色
            Bitmap newImage = MakeBackgroundTransparent(originalImage2, transparentColor);
            newImage.Save("E:\\图片\\WebApplication\\WebApplication\\images\\透明.png", System.Drawing.Imaging.ImageFormat.Png);
            #endregion


            #region 图片变白
            Bitmap originalImage1 = (Bitmap)System.Drawing.Image.FromFile("E:\\图片\\WebApplication\\WebApplication\\images\\2.jpg");
            Bitmap processedImage = ChangeGrayBackgroundToWhite(originalImage1);
            processedImage.Save("E:\\图片\\WebApplication\\WebApplication\\images\\白.jpg");
            #endregion

        }


        /// <summary>
        /// 背景改成白色
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public Bitmap ChangeGrayBackgroundToWhite(Bitmap source)
        {
            // 创建一个新的位图，用于存储处理后的图片
            Bitmap newBitmap = new Bitmap(source.Width, source.Height);

            // 创建Graphics对象以便于处理图片
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                // 设置背景颜色为白色
                g.Clear(Color.White);

                // 定义一个矩形，该矩形代表源图片将要被处理的部分
                Rectangle rect = new Rectangle(0, 0, source.Width, source.Height);

                // 定义一个矩阵，该矩阵用于设置图片的像素
                float[][] matrixItems =
                {
            new float[] {1, 0, 0, 0, 0}, // 红色系数
            new float[] {0, 1, 0, 0, 0}, // 绿色系数
            new float[] {0, 0, 1, 0, 0}, // 蓝色系数
            new float[] {0, 0, 0, 1, 0}, // Alpha不透明度系数
            new float[] {0, 0, 0, 0, 1}  // 偏移量
        };

                ColorMatrix colorMatrix = new ColorMatrix(matrixItems);

                ImageAttributes imageAttributes = new ImageAttributes();
                imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                // 绘制新的图片，此时源图片的灰色部分将被忽略
                g.DrawImage(source, rect, 0, 0, source.Width, source.Height, GraphicsUnit.Pixel, imageAttributes);
            }

            return newBitmap;
        }


        /// <summary>
        /// 背景改透明
        /// </summary>
        /// <param name="img"></param>
        /// <param name="transparentColor"></param>
        /// <returns></returns>
        public Bitmap MakeBackgroundTransparent(System.Drawing.Image img, Color transparentColor)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                // 设置透明色
                g.FillRectangle(new SolidBrush(Color.Transparent), 0, 0, bmp.Width, bmp.Height);

                // 确定透明区域
                Color[] transparentPixels = new Color[img.Width * img.Height];
                Bitmap transparentArea = new Bitmap(img.Width, img.Height);
                for (int y = 0; y < img.Height; y++)
                {
                    for (int x = 0; x < img.Width; x++)
                    {
                        Color originalColor = ((Bitmap)img).GetPixel(x, y);
                        if (originalColor == transparentColor)
                        {
                            transparentPixels[x + (y * img.Width)] = Color.Transparent;
                        }
                        else
                        {
                            transparentPixels[x + (y * img.Width)] = originalColor;
                        }
                    }
                }
                // 应用透明区域
                transparentArea.SetResolution(img.HorizontalResolution, img.VerticalResolution);
                using (Graphics g2 = Graphics.FromImage(transparentArea))
                {
                    g2.DrawImageUnscaled(bmp, 0, 0);
                }
                bmp = transparentArea;
            }
            return bmp;
        }


        /// <summary>
        /// 彩色图片灰度
        /// </summary>
        /// <param name="original"></param>
        /// <returns></returns>
        public static Bitmap MakeGrayscale(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);
            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);
            //create the grayscale ColorMatrix
            System.Drawing.Imaging.ColorMatrix colorMatrix = new System.Drawing.Imaging.ColorMatrix(
             new float[][]
             {
             new float[] {.3f, .3f, .3f, 0, 0},
             new float[] {.59f, .59f, .59f, 0, 0},
             new float[] {.11f, .11f, .11f, 0, 0},
             new float[] {0, 0, 0, 1, 0},
             new float[] {0, 0, 0, 0, 1}
             });
            //create some image attributes
            System.Drawing.Imaging.ImageAttributes attributes = new System.Drawing.Imaging.ImageAttributes();
            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);
            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
 0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);
            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }

        /// <summary>
        /// 背景色改为白色
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="outputPath"></param>

        public void ChangeBackgroundToWhite(string imagePath, string outputPath)
        {
            // 加载图片
            using (Bitmap bmp = new Bitmap(imagePath))
            {
                // 遍历每个像素
                for (int y = 0; y < bmp.Height; y++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        // 获取当前像素的颜色
                        Color pixelColor = bmp.GetPixel(x, y);

                        // 如果像素的颜色是透明的或者你想要更改的颜色，则设置为白色
                        if (pixelColor.A == 0 || pixelColor == Color.Transparent)
                        {
                            bmp.SetPixel(x, y, Color.White);
                        }
                    }
                }

                // 保存新的图片
                bmp.Save(outputPath);
            }
        }

        /// <summary>
        /// 改变图片对比度代码
        /// </summary>
        /// <param name="image"></param>
        /// <param name="contrast"></param>
        /// <returns></returns>
        public Bitmap AdjustContrast(System.Drawing.Image image, float contrast)
        {
            // 创建一个Bitmap对象，并从原始图片复制像素数据
            Bitmap adjustedBitmap = new Bitmap(image.Width, image.Height);

            // 创建一个Graphics对象来绘制调整对比度后的图片
            using (Graphics g = Graphics.FromImage(adjustedBitmap))
            {
                // 创建一个新的ColorMatrix，并设置对比度
                ColorMatrix colorMatrix = new ColorMatrix(new float[][]
                {
            new float[] {contrast, 0, 0, 0, 0},
            new float[] {0, contrast, 0, 0, 0},
            new float[] {0, 0, contrast, 0, 0},
            new float[] {0, 0, 0, 1, 0},
            new float[] {0, 0, 0, 0, 1}
                });

                // 创建ImageAttributes对象并设置其ColorMatrix
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(colorMatrix);

                // 绘制调整对比度后的图片
                g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height),
                            0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
            }

            return adjustedBitmap;
        }



        /// <summary>
        /// 将检查每个像素的颜色，如果它的灰度值在某个阈值范围内，则将其替换为白色。
        /// </summary>
        /// <param name="originalImage"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public Bitmap ChangeGrayBackgroundToWhite(Bitmap originalImage, int threshold)
        {
            // 创建原图的副本，以免修改原图
            Bitmap modifiedImage = new Bitmap(originalImage);

            // 获取图片的宽度和高度
            int width = modifiedImage.Width;
            int height = modifiedImage.Height;

            // 遍历图片的每个像素
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // 获取当前像素的颜色
                    Color originalColor = modifiedImage.GetPixel(x, y);

                    // 计算当前颜色的灰度值
                    int grayValue = (int)(originalColor.R * 0.3 + originalColor.G * 0.59 + originalColor.B * 0.11);

                    // 如果灰度值在阈值范围内，则替换为白色
                    if (grayValue < threshold)
                    {
                        modifiedImage.SetPixel(x, y, Color.White);
                    }
                }
            }

            return modifiedImage;
        }


    }
}